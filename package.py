import shutil
import subprocess

def bundle():
    # Index.html is generated in the build section.
    shutil.copy("src/index.html", "public/index.html")
    shutil.copy("src/404.html", "public/404.html")

def build():
    # Run tailwind to generate css.
    subprocess.run(["tailwindcss", "-o", "../public/output.css",  "--minify"], cwd="src")

def main():
    build()
    bundle()
    print("Done packaging + building for release.")
    

if __name__ == "__main__":
    main()
